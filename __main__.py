
import os
import sys
from taurus.external.qt import Qt
from taurus.qt.qtgui.panel import TaurusDevicePanel
import numpy as np
from taurus.qt.qtgui.application import TaurusApplication
from taurus_pyqtgraph import TaurusPlot
from taurus.qt.qtgui.panel import TaurusForm
from PyQt5.QtCore import Qt as Qt2
from taurus.qt.qtgui.display import TaurusLabel
from svgsynoptic2.taurussynopticwidget import TaurusSynopticWidget


class ExampleSynopticWidget(TaurusSynopticWidget):

    """
    A custom subclass of the synoptic widget.
    """

    def get_device_panel(self, device):
        return TaurusDevicePanel


def main():

    qapp = TaurusApplication(sys.argv, cmd_line_parser=None)
    panel = np.array([Qt.QDockWidget, Qt.QDockWidget, Qt.QDockWidget, Qt.QDockWidget])
    qm = Qt.QMainWindow()
    toolbar = qm.addToolBar('Opciones')
    icon = Qt.QIcon.fromTheme("document-close")
    toolbar.addAction(icon, "Cerrar", qapp.closeAllWindows)
    toolbar.setToolButtonStyle(4)
    rlj = TaurusLabel()
    rlj.setModel('gc/clock/1/time')
    toolbar.addWidget(rlj)
    toolbar.addAction(Qt.QIcon.fromTheme('document-open'), 'Acerca de Qt', qapp.aboutQt)
    for i in range(4):
        panel[i] = Qt.QDockWidget()
        
    plt = TaurusPlot()
    plt.setModel('bl06/ct/eps-plc-01/AnalogIntsREAD')
    plt2 = TaurusPlot()
    plt2.setModel('bl06/ct/eps-plc-01/AnalogRealsREAD')
    form = TaurusForm()
    form.setModel(['bl06/ct/eps-plc-01/AUX_PT', 'bl06/ct/eps-plc-01/AUX_TEMPERATURE_AF','bl06/ct/eps-plc-01/CPU_T1', 'bl06/ct/eps-plc-01/CPU_T2' ])
    path = os.path.dirname(__file__)
    widget = ExampleSynopticWidget()
    widget.setConfig(os.path.join(path, "models.json"))
    widget.setModel(os.path.join(path, "base.html"))
    widget.resize(2000, 1700)
    
    panel[3].setWidget(plt)
    panel[1].setWidget(plt2)
    panel[2].setWidget(form)
    panel[0].setWidget(widget)
    panel[0].setMinimumWidth(300)
    panel[0].setMinimumHeight(300)
    for j in range(2):
        qm.addDockWidget(Qt2.DockWidgetArea.TopDockWidgetArea, panel[j])

    for k in range(2):
        qm.addDockWidget(Qt2.DockWidgetArea.BottomDockWidgetArea, panel[k+2])
        
    qm.show()
    qapp.exec_()

if __name__ == "__main__":
    main()