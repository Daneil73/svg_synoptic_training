"""Just needed to execute one time to poll the attributes needed.
"""

import fandango
fandango.tango.set_attribute_events('bl06/ct/eps-plc-01/CPU_T1',polling=1000,rel_event=0.01)
fandango.tango.set_attribute_events('bl06/ct/eps-plc-01/CPU_T2',polling=1000,rel_event=0.01)